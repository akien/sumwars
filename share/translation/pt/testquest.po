# Portuguese translations for PACKAGE package
# Copyright (C) 2012 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package
#  <kalimgard@users.sourceforge.net>, 2012
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-03-09 15:15+0100\n"
"PO-Revision-Date: 2018-09-20 02:33+0000\n"
"Last-Translator: Henrique Combochi <henrique.combochi@gmail.com>\n"
"Language-Team: Portuguese <https://hosted.weblate.org/projects/sumwars/"
"testquest/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.2-dev\n"

#: testquest.xml:2
msgid "Fenor Marken"
msgstr ""

#: testquest.xml:2
msgid "testquest"
msgstr ""

#: testquest.xml:3
msgid "Test"
msgstr "Teste"

#: testquest.xml:9
msgid "This is a Test"
msgstr "Isso é um Teste"

#: testquest.xml:11
msgid "surprised"
msgstr "surpreso"

#: testquest.xml:13
msgid "thoughtful"
msgstr "pensativo"

#: testquest.xml:16
msgid "happy"
msgstr "feliz"

#: testquest.xml:19
msgid "amused"
msgstr "entretido"

#: testquest.xml:21
msgid "excited"
msgstr "excitado"

#: testquest.xml:23
msgid "in love"
msgstr "apaixonado"

#: testquest.xml:25
msgid "bored"
msgstr "entediado"

#: testquest.xml:27
msgid "unhappy"
msgstr "infeliz"

#: testquest.xml:29
msgid "sad"
msgstr "triste"

#: testquest.xml:31
msgid "pain"
msgstr "dor"

#: testquest.xml:33
msgid "sneer"
msgstr "zombar"

#: testquest.xml:35
msgid "grin"
msgstr "sorriso"

#: testquest.xml:37
msgid "offended"
msgstr "ofendido"

#: testquest.xml:39
msgid "threatening"
msgstr "ameaçando"

#: testquest.xml:41
msgid "angry"
msgstr "nervoso"

#: testquest.xml:43
msgid "shouting a warcry"
msgstr "gritando um grito de guerra"

#: testquest.xml:45
msgid "exhausted"
msgstr "exausto"

#: testquest.xml:47
msgid "injured"
msgstr "machucado"

#: testquest.xml:49
msgid "deadly injured"
msgstr "mortalmente ferido"

#: testquest.xml:50
msgid "afraid"
msgstr "amedontrado"

#: testquest.xml:51
msgid "in panic"
msgstr "em pânico"

#: testquest.xml:52
msgid "putting up a distance"
msgstr "distanciando"

#: testquest.xml:53
msgid "being disgusted"
msgstr "ficando com nojo"

#: testquest.xml:54
msgid "and yet another Test"
msgstr ""

#: testquest.xml:55
msgid "again a test"
msgstr "novamente um teste"
